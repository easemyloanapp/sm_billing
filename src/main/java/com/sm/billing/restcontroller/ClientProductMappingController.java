package com.sm.billing.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sm.billing.datalayer.model.ProductMaster;
import com.sm.billing.service.ClientProcudtMappingService;

@RestController
public class ClientProductMappingController {

	@Autowired
	private ClientProcudtMappingService service;
	
	
	@PostMapping("/bil/internal/billing/getDetails")
	public List<ProductMaster> getClientProductDetails(@RequestBody String ClientId) {
		List<ProductMaster> list = service.getClientProductDetails(ClientId);
		return list;
		
	}
}
