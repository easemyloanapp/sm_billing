package com.sm.billing.restcontroller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sm.billing.datalayer.model.RateCard;
import com.sm.billing.datalayer.model.Request;
import com.sm.billing.datalayer.model.ValidationResponse;
import com.sm.billing.service.RateCardService;

@RestController
@Component
public class RateCardController {

	@Autowired
	private RateCardService service;

	// adding the fiels to the ratecard table
	@PostMapping("/bil/internal/billing/addratecard")
	public ValidationResponse addRateCard(@RequestBody RateCard card) {
		ValidationResponse responce = new ValidationResponse();

		if (card.getMinvolume() == null && card.getMaxvolume() != null) {
			card.setMinvolume(0);
		}
			// checking the validations for min and max values minvolume is lessthen
			// maxvolume
			if (card.getMinvolume() < card.getMaxvolume()) {
				// checking the validations for dates
				if (card.getValidfrom().before(card.getValidtill())) {
					ValidationResponse r = service.addRateCard(card);
					responce.setData(card);
					responce.setResponseCode(r.getResponseCode());
					responce.setResponseMessage(r.getResponseMessage());
				} else {
					responce.setResponseCode("400");
					responce.setResponseMessage("to date must be high");
				}
			} else {
				responce.setResponseCode("500");
				responce.setResponseMessage("maximum value must be high");
			}


		return responce;
	}

	// here client pass volume and current the we can get chargePerTransaction
	@PostMapping("/bil/internal/billing/get")
	public ValidationResponse getchargePertransaction(@RequestBody Request request) {
		ValidationResponse responce = new ValidationResponse();
		ValidationResponse r = service.getChargePerTransaction(request);
		responce.setData(r.getData());
		responce.setResponseCode(r.getResponseCode());
		responce.setResponseMessage(r.getResponseMessage());
		return responce;
	}

	// by using id delete the entire row in the table
	@DeleteMapping(value = "/bil/internal/billing/deletebyid/{id}")
	public ValidationResponse deleteDatafromtable(@PathVariable int id) throws IOException {
		ValidationResponse responce = new ValidationResponse();
		if (id > 0) {
			ValidationResponse r = service.deleteRateCard(id);
			responce.setResponseCode(r.getResponseCode());
			responce.setResponseMessage(r.getResponseMessage());
		} else {
			responce.setResponseCode("400");
			responce.setResponseMessage("value is not deleted");
		}
		return responce;
	}

	// except client_id update the remaining values
	@PostMapping("/bil/internal/billing/update")
	public ValidationResponse updateBilling(@RequestBody RateCard card) {
		ValidationResponse responce = new ValidationResponse();

		if (card.getId() != 0) {
			ValidationResponse r = service.updateRateCard(card);
			responce.setData(r.getData());
			responce.setResponseCode(r.getResponseCode());
			responce.setResponseMessage(r.getResponseMessage());
		} else {
			responce.setResponseCode("500");
			responce.setResponseMessage("values are not updated");

		}
		return responce;
	
	    }
}
