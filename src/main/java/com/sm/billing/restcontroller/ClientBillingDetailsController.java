package com.sm.billing.restcontroller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sm.billing.datalayer.model.ClientBillingDetails;
import com.sm.billing.service.ClientBillingDetailsService;

@RestController
//@RequestMapping("/clientBilling")
public class ClientBillingDetailsController {

	@Autowired
	private ClientBillingDetailsService service;

	// based on clientId ,get the billingdetails
	@GetMapping("/bil/internal/billing/getbillingadata")
	public Optional<ClientBillingDetails> getBillingDetails(@RequestParam String clientId,@RequestParam Boolean deleted) {
		
		return service.getBillingDetails(clientId,deleted);
	}

	@PostMapping("/bil/internal/billing/addbillingdata")
	public ClientBillingDetails addBilling(@RequestBody ClientBillingDetails details) {
		return service.addBillingDetails(details);
	}

}
