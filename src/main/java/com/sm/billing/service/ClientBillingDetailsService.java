package com.sm.billing.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sm.billing.datalayer.model.ClientBillingDetails;
import com.sm.billing.datalayer.repository.ClientBillingDetailsRepo;

@Service
public class ClientBillingDetailsService {

	@Autowired
	private ClientBillingDetailsRepo repo;

	public Optional<ClientBillingDetails> getBillingDetails(String clinetId,Boolean deleted) {
		return repo.findBYClientId(clinetId,deleted);

	}

	public ClientBillingDetails addBillingDetails(ClientBillingDetails details) {

		return repo.save(details);
	}

}
