package com.sm.billing.service;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sm.billing.datalayer.model.RateCard;
import com.sm.billing.datalayer.model.Request;
import com.sm.billing.datalayer.model.ValidationResponse;
import com.sm.billing.datalayer.repository.RateCardRepo;

@Service
public class RateCardService {

	@Autowired
	private RateCardRepo repo;

	public ValidationResponse addRateCard(RateCard card) {
		ValidationResponse responce = new ValidationResponse();
		LocalDate ld = LocalDate.now();
		card.setCreated(ld);
		Integer str = repo.checkMinMaxVolumes(card.getClientid(),card.getMinvolume(), card.getMaxvolume(), card.getValidfrom(), card.getValidtill());
		if((str != null && str == 1)) {
			responce.setResponseCode("500");
			responce.setResponseMessage("minvolume and maximunvolume alredy exist");
			return responce;
		} else {
			repo.save(card);
			responce.setResponseCode("200");
			responce.setResponseMessage("data saved to ratecard");
		}
		
		return responce;

	}

	public ValidationResponse deleteRateCard(int id) {
		ValidationResponse responce = new ValidationResponse();
		RateCard card = repo.findById(id).orElse(null);
		if (card != null) {
			repo.deleteById(id);
			responce.setResponseCode("200");
			responce.setResponseMessage("values are deleted");
		} else {
			responce.setResponseCode("500");
			responce.setResponseMessage("values are not found");
		}
		return responce;
	}

	public ValidationResponse updateRateCard(RateCard card) {

		RateCard card1 = repo.findById(card.getId()).orElse(null);
		ValidationResponse responce = new ValidationResponse();

		if (card1 != null) {
			
			if(card.getMinvolume() != null) {
				card1.setMinvolume(card.getMinvolume());
			}
			if(card.getMaxvolume() != null) {
				card1.setMaxvolume(card.getMaxvolume());
			}
			if(card.getNote() != null) {
				card1.setNote(card.getNote());
			}
			if(card.getValidfrom() != null) {
				card1.setValidfrom(card.getValidfrom());
			}
			if(card.getValidtill() != null) {
				card1.setValidtill(card.getValidtill());
			}
			if(card.getChargepertransaction() != null) {
				card1.setChargepertransaction(card.getChargepertransaction());
			}	
			if(card.getProductid() != null) {
				card1.setProductid(card.getProductid());
			}
			LocalDate now = LocalDate.now();
			card1.setUpdated(now);
			repo.save(card1);
			responce.setData(card1);
			responce.setResponseCode("200");
			responce.setResponseMessage("updated");

		} else {
			responce.setResponseCode("400");
			responce.setResponseMessage(" ID not found");
		}
		return responce;
	}

	public ValidationResponse getChargePerTransaction(Request request) {

		ValidationResponse responce = new ValidationResponse();
		Float chargepertransaction = repo.getChargepertransactionValue(request.getClientId(), request.getDate(), request.getVolume());
		if(chargepertransaction != null) {
			responce.setData(chargepertransaction);
			responce.setResponseCode("200");
			responce.setResponseMessage("fetched chargePerTransaction");
		} else {
			responce.setResponseCode("500");
			responce.setResponseMessage("Details Not Found");
		}
		return responce;

	}


}
