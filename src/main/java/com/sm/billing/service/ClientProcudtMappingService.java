package com.sm.billing.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sm.billing.datalayer.model.ProductMaster;
import com.sm.billing.datalayer.repository.ProductMasterRepo;

@Service
public class ClientProcudtMappingService {
	
	@Autowired
	private  ProductMasterRepo repo;
	
	
	public List<ProductMaster> getClientProductDetails(String ClientId ) {
		List<ProductMaster> list =	repo.getProductDetailsBasedOnClientId(ClientId);
		return list;
		
	}
}
