package com.sm.billing.datalayer.model;

import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Table(name = "client_ratecard")
public class RateCard {

	@Id
	@GeneratedValue
	private Integer id;
	@Column(name = "clientid", unique = true, nullable = true)
	private String clientid;
	@Column(name = "minvolume")
	private Integer minvolume;
	@Column(name = "maxvolume")
	private Integer maxvolume;

	private Date validfrom;

	private Date validtill;

	@CreatedDate
	private LocalDate created;

	@LastModifiedDate
	private LocalDate updated;

	private Boolean deleted;
	private String note;
	private Float chargepertransaction;
	private Integer productid;

	
	public Integer getProductid() {
		return productid;
	}

	public void setProductid(Integer productid) {
		this.productid = productid;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClientid() {
		return clientid;
	}

	public void setClientid(String clientid) {
		this.clientid = clientid;
	}

	public Integer getMinvolume() {
		return minvolume;
	}

	public void setMinvolume(Integer minvolume) {
		this.minvolume = minvolume;
	}

	public Integer getMaxvolume() {
		return maxvolume;
	}

	public void setMaxvolume(Integer maxvolume) {
		this.maxvolume = maxvolume;
	}

	public Date getValidfrom() {
		return validfrom;
	}

	public void setValidfrom(Date validfrom) {
		this.validfrom = validfrom;
	}

	public Date getValidtill() {
		return validtill;
	}

	public void setValidtill(Date validtill) {
		this.validtill = validtill;
	}

	public LocalDate getCreated() {
		return created;
	}

	public void setCreated(LocalDate created) {
		this.created = created;
	}

	public LocalDate getUpdated() {
		return updated;
	}

	public void setUpdated(LocalDate updated) {
		this.updated = updated;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Float getChargepertransaction() {
		return chargepertransaction;
	}

	public void setChargepertransaction(Float chargepertransaction) {
		this.chargepertransaction = chargepertransaction;
	}

}