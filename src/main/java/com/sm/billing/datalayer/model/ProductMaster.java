package com.sm.billing.datalayer.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Table(name = "product_master")
public class ProductMaster { // product

	@Id
	private Integer id;
	@Column(name = "product_name")
	private String productName;
	@Column(name = "sheet_name")
	private String sheetName;
	@CreatedDate
	private Date created;
	@LastModifiedDate
	private Date modifed;
	private Boolean deleted;
	@Column(name = "hsn_sac")
	private String hsnSac;
	@Column(name = "igst_tax")
	private Integer igstTax;
	private String description;
	@Column(name = "productId")
	private Integer product_id;
	@Column(name = "cgst_tax")
	private Integer cgstTax;
	@Column(name = "sgst_tax")
	private Integer sgstTax;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSheetName() {
		return sheetName;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModifed() {
		return modifed;
	}

	public void setModifed(Date modifed) {
		this.modifed = modifed;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getHsnSac() {
		return hsnSac;
	}

	public void setHsnSac(String hsnSac) {
		this.hsnSac = hsnSac;
	}

	public Integer getIgstTax() {
		return igstTax;
	}

	public void setIgstTax(Integer igstTax) {
		this.igstTax = igstTax;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}

	public Integer getCgstTax() {
		return cgstTax;
	}

	public void setCgstTax(Integer cgstTax) {
		this.cgstTax = cgstTax;
	}

	public Integer getSgstTax() {
		return sgstTax;
	}

	public void setSgstTax(Integer sgstTax) {
		this.sgstTax = sgstTax;
	}

	public ProductMaster() {
		super();
	}

}
