package com.sm.billing.datalayer.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Table(name = "client_invoice_items")
public class ClientInvoiceItems {

	@Id
	private Integer id;
	@Column(name = "client_invoice_id")
	private Integer clientInvoiceId;
	@Column(name = "product_id")
	private Integer productId;
	@Column(name = "product_name")
	private String productName;
	@Column(name = "product_description")
	private String productDescription;
	@Column(name = "hsn_sac")
	private String hsnSac;
	private Integer igst;
	private Double rate;
	private Integer quantity;
	@CreatedDate
	private Date created;
	@LastModifiedDate
	private Date modified;
	private Boolean deleted;
    private Integer cgst;
    private Integer sgst;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getClientInvoiceId() {
		return clientInvoiceId;
	}
	public void setClientInvoiceId(Integer clientInvoiceId) {
		this.clientInvoiceId = clientInvoiceId;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	public String getHsnSac() {
		return hsnSac;
	}
	public void setHsnSac(String hsnSac) {
		this.hsnSac = hsnSac;
	}
	public Integer getIgst() {
		return igst;
	}
	public void setIgst(Integer igst) {
		this.igst = igst;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}
	public Boolean getDeleted() {
		return deleted;
	}
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	public Integer getCgst() {
		return cgst;
	}
	public void setCgst(Integer cgst) {
		this.cgst = cgst;
	}
	public Integer getSgst() {
		return sgst;
	}
	public void setSgst(Integer sgst) {
		this.sgst = sgst;
	}
	public ClientInvoiceItems() {
		super();
	}
    
    
	
}
