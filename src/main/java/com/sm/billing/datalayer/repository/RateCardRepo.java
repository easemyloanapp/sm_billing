package com.sm.billing.datalayer.repository;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.sm.billing.datalayer.model.RateCard;

@Repository
@Component
public interface RateCardRepo extends JpaRepository<RateCard, Integer>{
	
	
	//using query get the chargePerTransaction
	@Transactional
	@Query(value = "select chargepertransaction " + " from billing.client_ratecard "
            + "    where clientid = :clientId and minvolume <= :volume and maxvolume >= :volume and validfrom < :date and validtill > :date", nativeQuery = true)
    Float  getChargepertransactionValue(@Param("clientId") String clientId, @Param("date") Date date,  @Param("volume") Integer volume );
	
	
	/*while inserting data to the table follow same validations
	 * if user not enter any minimum values it will be null
	 *  date validations
	 */
	
	@Transactional
	@Query(value="select 1 " + " from client_ratecard where clientid= :clientid "
			+ "AND ((:minv is NULL AND IFNULL(minvolume,0)=0) "
			+ "OR(:minv is NOT NULL AND :minv BETWEEN IFNULL(minvolume,0) AND  IFNULL(maxvolume,:maxv) ) "
			+ "OR(:maxv is NULL AND maxvolume is NULL) "
			+ "OR(:maxv is NOT NULL AND :maxv BETWEEN IFNULL(minvolume,0) AND  IFNULL(maxvolume,:maxv) ) "
			+ ")AND( (:mind is NULL AND validfrom is null)OR "
			+ "(:mind is NOT NULL AND :mind BETWEEN IFNULL(validfrom,:mind) AND  IFNULL(validtill,:mind) )OR"
			+ "(:maxd is NULL AND validtill is null)OR "
			+ "(:maxd is NOT NULL AND :maxd BETWEEN IFNULL(validfrom,:maxd) AND  IFNULL(validtill,:maxd) )) ;"
			,nativeQuery=true)
    Integer   checkMinMaxVolumes(@Param("clientid")String clientid,  @Param("minv") Integer minv, @Param("maxv") Integer maxv, @Param("mind") Date mind, @Param("maxd") Date maxd );

	
}
