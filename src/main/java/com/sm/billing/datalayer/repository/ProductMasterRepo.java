package com.sm.billing.datalayer.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sm.billing.datalayer.model.ProductMaster;

@Repository
public interface ProductMasterRepo extends JpaRepository<ProductMaster, Integer> {

	// select * from billing.product_master where product_id in (select product_id
	// from billing.client_product_mapping where client_id = 'ABC')
	@Transactional
	@Query(value = "select * from billing.product_master where product_id IN (select product_id from  billing.client_product_mapping where client_id = :clientId)", nativeQuery = true)
	public List<ProductMaster> getProductDetailsBasedOnClientId(@Param("clientId") String clientId);

}
