package com.sm.billing.datalayer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.sm.billing.datalayer.model.ClientInvoiceItems;

@Repository
public interface ClientInvoiceItemsRepo extends JpaRepository<ClientInvoiceItems, Integer>{

}
