package com.sm.billing.datalayer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sm.billing.datalayer.model.ClientProductMapping;

@Repository
public interface ClientProductMappingRepo extends JpaRepository<ClientProductMapping, Integer>{
	
}
