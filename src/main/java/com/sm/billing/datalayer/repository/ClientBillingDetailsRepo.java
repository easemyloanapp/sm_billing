package com.sm.billing.datalayer.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sm.billing.datalayer.model.ClientBillingDetails;

@Repository
public interface ClientBillingDetailsRepo extends JpaRepository<ClientBillingDetails, Integer> {

//	SELECT * FROM billing.client_billing_details WHERE client_id = 'ABCD' AND deleted = '0';

	@Query(value = "SELECT * FROM billing.client_billing_details WHERE client_id = :clientId AND deleted = :deleted", nativeQuery = true)

	Optional<ClientBillingDetails> findBYClientId(@Param("clientId") String clinetId,
			@Param("deleted") Boolean deleted);

}
